---
title: "Lage | Sekretariat"
title-meta: "Kontakt"
description: ""
header: head12.jpg
image:
  file: blum_Gebaeude.jpg
  width: 225
  height: 300
narrow: true;
---

Anwältinnen  
Bichsel, Blum & Fürst  
Blumenrain 3  
4051 Basel

<p class="email-paragraph"><a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#105;&#110;&#x66;&#111;&#64;&#x61;&#110;&#x77;&#x61;&#101;&#x6C;&#x74;&#105;&#x6E;&#110;&#101;&#x6E;&#x2D;&#x62;&#x61;&#x73;&#101;&#108;&#x2E;&#99;&#x68;">&#105;&#110;&#x66;&#111;&#64;&#x61;&#110;&#x77;&#x61;&#101;&#x6C;&#x74;&#105;&#x6E;&#110;&#101;&#x6E;&#x2D;&#x62;&#x61;&#x73;&#101;&#108;&#x2E;&#99;&#x68;</a></p>

Telefon +41 (0)61 269 98 00  
Telefax +41 (0)61 269 98 09

<a href="https://map.search.ch/Basel,Blumenrain-3" target="_blank">Lageplan</a>

Der erste Kontakt findet oft auch mit unserem Sekretariat statt, welches unsere Tätigkeit unterstützt und ergänzt. Frau Carolin Krupke leitet unser Sekretariat und ist gern für Sie da.
