---
title: "Anwältinnen"
title-meta: "Anwältinnen Basel, Bichsel, Blum &amp; Fürst"
description: "Bichsel, Blum &amp; Fürst - Rechtsanwältinnen in Basel für Familienrecht, Mietrecht, Opferhilferecht, Sozialversicherungsrecht, Arbeitsrecht, AusländerInnenrecht, Haftpflichtrecht, Erbrecht"
header: head14.jpg
---

## Kathrin Bichsel

lic. iur. Advokatin, Mediatorin SAV, CAS IRP-HSG in Haftpflicht- und Versicherungsrecht  
Arbeitsrecht, Mietrecht, Sozialversicherungsrecht, Opferhilferecht, Haftpflichtrecht, Familienrecht, Eingetragene Partnerschaften, [...mehr](/kathrin-bichsel)

## Nina Blum

Dr. iur. Advokatin, LL.M.  
Familienrecht (inkl. Kindesvertretung, Kindesschutz), Strafrecht, Opferhilferecht, Migrationsrecht, Mietrecht, [...mehr](/nina-blum)

## Catherine Fürst

lic. iur. Advokatin  
Familienrecht, Strafrecht, Opferhilferecht, Erbrecht, [...mehr](/catherine-fuerst)
