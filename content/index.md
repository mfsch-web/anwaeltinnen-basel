---
title: "Anwältinnen Bichsel, Blum &amp; Fürst"
title-meta: "Bichsel, Blum &amp; Fürst - Rechtsanwältinnen, Basel: Familienrecht, Mietrecht, Opferhilferecht"
description: "Bichsel, Blum &amp; Fürst - Rechtsanwältinnen in Basel für Familienrecht, Mietrecht, Opferhilferecht, Sozialversicherungsrecht, Arbeitsrecht, AusländerInnenrecht, Haftpflichtrecht, Erbrecht"
header: head11.jpg
---

Sie suchen kompetente Beratung und Unterstützung in Rechtsfragen? Wir stehen Ihnen gerne zur Seite. Als erfahrene Anwältinnen (unser Büro besteht seit 1985) beraten wir Sie in rechtlichen Angelegen­heiten, geben Ihnen Auskunft über Rechtslage und -risiken, vertreten Ihre Interessen in Verhandlungen, vor Gerichten und gegenüber Behörden. Wenn möglich und gewünscht, engagie­ren wir uns für aussergerichtliche Lösungen und bieten auch Mediation an.

In diesen Fachgebieten sind wir spezialisiert:

- Familienrecht (inkl. internationales Privatrecht)
- Mietrecht
- Opferhilferecht
- Sozialversicherungsrecht
- Haftpflichtrecht
- Strafrecht in ausgesuchten Fällen
- Migrationsrecht
