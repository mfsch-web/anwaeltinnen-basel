---
title: "Catherine Fürst"
title-meta: "Catherine Fürst"
description: "Catherine Fürst, Rechtsanwältin, Familienrecht, Strafrecht, Opferhilferecht, Erbrecht, Sozialversicherungsrecht"
header: HeadCF_1.jpg
nav-anwaeltinnen: true
image:
  file: CatherineFuerst_2-a49932e7.jpg
  full: CatherineFuerst_2.jpg
---

<p class="email"><a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#99;&#46;&#102;&#x75;&#101;&#x72;&#115;&#116;&#x40;&#97;&#x6E;&#119;&#97;&#101;&#x6C;&#x74;&#105;&#110;&#x6E;&#x65;&#x6E;&#x2D;&#x62;&#x61;&#115;&#101;&#108;&#x2E;&#99;&#104;">&#99;&#46;&#102;&#x75;&#101;&#x72;&#115;&#116;&#x40;&#97;&#x6E;&#119;&#97;&#101;&#x6C;&#x74;&#105;&#110;&#x6E;&#x65;&#x6E;&#x2D;&#x62;&#x61;&#115;&#101;&#108;&#x2E;&#99;&#104;</a></p>

- 1 erwachsene Tochter, 2 Enkel
- 1974 Advokaturexamen Basel-Stadt
- 1977 Eröffnung Advokaturbüro Weisse Gasse 15, Basel
- regelmässige Teilnahme an Kursen und Kongressen zu verschiedenen Rechtsgebieten zur ständigen Weiterbildung z.B.
  - interdisziplinäre Arbeitsgruppe zu familienrechtlicher Mediation
  - Kriminologie und Strafrecht
  - Ausbildung Kindsvertretung
  - Kurs Universität Zürich zu Intervention und Prävention bei sexueller Gewalt
  - Verhandlungsführung

## Bevorzugte Arbeitsgebiete

- grundsätzlich Allrounderin, insbesonders
- Strafrecht
- Opferhilferecht
- Familienrecht
- Erbrecht
- Sozialversicherungsrecht

## Mitgliedschaften und weitere Tätigkeiten

- Advokatenkammer Basel-Stadt und Schweizerischer Anwaltsverband
- Demkokratische JuristInnen Basel/Schweiz
- Vorstand Verein Pikett Strafverteidigung
- BEOBACHTER AnwaltsnetzPräsidentin Stiftung Suchthilfe Region Basel

## Sprachen

Deutsch, Französisch, Englisch, Italienisch
