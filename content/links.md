---
title: "Links"
title-meta: "Links"
description: ""
header: head7.jpg
---

Der **MieterInnenverband BS** setzt sich für Ihre Anliegen als Mieterin oder Mieter ein:  
<a href="http://www.mieterverband.ch/bs" target="_blank">MBVS</a>

Die **Opferhilfe beider Basel** unterstützt Sie als Betroffene von Gewaltdelikten:  
<a href="http://www.opferhilfe-beiderbasel.ch/" target="_blank">Opferhilfe beider Basel</a>
