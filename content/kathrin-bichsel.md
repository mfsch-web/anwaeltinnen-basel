---
title: "Kathrin Bichsel"
title-meta: "Kathrin Bichsel"
description: "Kathrin Bichsel, Rechtsanwältin für Oeffentliches und privates Arbeitsrecht, insbesondere auch Diskriminierungsstreitigkeiten nach Gleichstellungsgesetz, Mietrecht ,Sozialversicherungsrecht (Unfall/Krankheit/Invalidität/Berufliche Vorsorge), Opferhilferecht, Haftpflichtrecht, Familienrecht (Scheidung, Trennung), Eingetragene Partnerschaft"
header: head8.jpg
nav-anwaeltinnen: true
image:
  file: kathrin_bichsel_web-805c7bed.jpg
  full: kathrin_bichsel_web.jpg
---

<p class="email"><a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x6B;&#46;&#98;&#x69;&#99;&#104;&#x73;&#101;&#x6C;&#x40;&#x61;&#x6E;&#119;&#x61;&#101;&#108;&#x74;&#x69;&#x6E;&#x6E;&#x65;&#110;&#x2D;&#x62;&#x61;&#115;&#x65;&#x6C;&#x2E;&#x63;&#x68;">&#x6B;&#46;&#98;&#x69;&#99;&#104;&#x73;&#101;&#x6C;&#x40;&#x61;&#x6E;&#119;&#x61;&#101;&#108;&#x74;&#x69;&#x6E;&#x6E;&#x65;&#110;&#x2D;&#x62;&#x61;&#115;&#x65;&#x6C;&#x2E;&#x63;&#x68;</a></p>

- 1976–1982 Studium an der Universität Basel, Lizentiat
- 1986 Advokaturexamen
- Div. Tätigkeiten u.a. als Juristische Mitarbeiterin bei der OeKK
- 1990–1992 Verbandssekretärin Mieterinnen- und Mieterverband BL
- 1992–1997 Rechtsanwältin im Rechtsdienst des Basler Gewerkschaftsbundes
- Seit 1. April 1997 Aufnahme selbständige Tätigkeit als Anwältin
- 2006/2007 Weiterbildung Universität St. Gallen in Haftpflicht- und Versicherungsrecht, Certificate of Advances Studies IPR-HSG in Torts and Insurance Law
- 2017/2018 Ausbildung als Mediatorin SAV
- Vertrauensanwältin Mieterinnen- und Mieterverband Basel-Stadt

## Bevorzugte Arbeitsgebiete

- Oeffentliches und privates Arbeitsrecht, insbesondere auch Diskriminierungs&shy;streitigkeiten nach Gleichstellungsgesetz
- Mietrecht
- Sozialversicherungsrecht (Unfall/<wbr>Krankheit/<wbr>Invalidität/<wbr>Berufliche Vorsorge)
- Opferhilferecht (Vertretung und Begleitung von Opfern im Strafverfahren/<wbr>Durchsetzung Schadenersatz- und Genugtuungsansprüche)
- Haftpflichtrecht
- Familienrecht
- Eingetragene Partnerschaft

## Mitgliedschaften und weitere Tätigkeiten

- Mitglied des Schweizerischen Anwaltsverbandes und der Advokatenkammer Basel-Stadt
- Mitglied der Demokratischen Juristinnen und Juristen der Schweiz
- Präsidentin Mieterinnen- und Mieterverband Basel-Stadt
- Rechtsberatung beim Mieterinnen- und Mieterverband Basel-Stadt
- Mitglied der Kantonalen Schlichtungsstelle für Diskriminierungsfragen Basel-Stadt
- 2000–2019 Universitätsexterne Vertrauensperson, „Schutz vor sexueller Belästigung am Arbeitsplatz und im Studium“, Universität Basel
- Dozentin für Arbeitsrecht an der FHNW Hochschule für Soziale Arbeit

## Sprachen

Deutsch, Französisch, Englisch, Italienisch
