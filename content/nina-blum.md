---
title: "Nina Blum"
title-meta: "Nina Blum"
description: "Nina Blum, Rechtsanwältin für Familienrecht (inkl. Kindesvertretung, Kindesschutz), Strafrecht, Opferhilferecht, Migrationsrecht, Mietrecht"
header: head4.jpg
nav-anwaeltinnen: true
image:
  file: nina-blum-150x200.jpg
  full: nina-blum.jpg
---

<p class="email"><a href="&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;&#x3A;&#x6E;&#x2E;&#x62;&#x6C;&#x75;&#x6D;&#x40;&#x61;&#x6E;&#x77;&#x61;&#x65;&#x6C;&#x74;&#x69;&#x6E;&#x6E;&#x65;&#x6E;&#x2D;&#x62;&#x61;&#x73;&#x65;&#x6C;&#x2E;&#x63;&#x68;">&#x6E;&#x2E;&#x62;&#x6C;&#x75;&#x6D;&#x40;&#x61;&#x6E;&#x77;&#x61;&#x65;&#x6C;&#x74;&#x69;&#x6E;&#x6E;&#x65;&#x6E;&#x2D;&#x62;&#x61;&#x73;&#x65;&#x6C;&#x2E;&#x63;&#x68;</a></p>

- Geboren 1980
- 2000–2006 Studium an der Universität Basel, Lizentiat
- 2009 Studium an der New York University, LL.M.
- 2009–2011 wissenschaftliche Assistentin von Prof. Dr. iur. Anne Peters
- 2012 Doktorat an der Universität Basel zur Europäischen Menschenrechtskonvention
- 2012–2015 Juristische Mitarbeiterin Eidgenössisches Departement für auswärtige Angelegenheiten (EDA) in Bern
- 2017 Advokaturexamen im Kanton Basel-Landschaft
- 2016–2018 Juristische Mitarbeiterin im Rechtsdienst des Regierungsrates Basel-Landschaft in Liestal
- Dezember 2017 Aufnahme der selbständigen Tätigkeit als Anwältin
- 2020 CAS an der Hochschule Luzern in Kindesvertretung/ Verfahrensbeistandschaft

## Bevorzugte Arbeitsgebiete

- Familienrecht (Scheidung, Trennung, inklusive Kindesschutz und Kindesvertretung)
- Migrationsrecht
- Mietrecht
- Öffentliches und privates Arbeitsrecht, insbesondere auch Diskriminierungs&shy;streitigkeiten nach Gleichstellungsgesetz
- Strafverteidigung und Opferhilferecht (auch von Jugendlichen)

## Mitgliedschaften und weitere Tätigkeiten


- Mitglied Schweizerischer Anwaltsverband und Advokatenkammer Basel-Stadt
- Mitglied Pikett Strafverteidigung
- Mitglied Kinderanwaltschaft Schweiz
- Mitglied Demokratische Juristinnen und Juristen der Schweiz
- Pro Jure (Vorstand)
- Vorsitzende der beratenden Kommission zum Schutz der sexuellen Integrität am Arbeitsplatz Basel-Landschaft
- Rechtsberatung Basler Gewerkschaftsbund
- Rechtsberatung Mieterinnen- und Mieterverband BS
- Dozentin FHNW, Strafrecht in der Praxis

## Sprachen

Deutsch, Englisch, Französisch, Spanisch (Grundkenntnisse)
