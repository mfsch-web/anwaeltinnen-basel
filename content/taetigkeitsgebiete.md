---
title: "Tätigkeitsgebiete"
title-meta: "Tätigkeitsgebiete"
description: ""
header: head13.jpg
---

Fragen Sie uns bereits am Telefon, ob Sie mit Ihrem Anliegen bei uns an der richti­gen Stelle sind. Sie können Ihre Anliegen und Rechtsfragen mit uns besprechen – und zwar in Deutsch, Franzö­sisch, Englisch, Italienisch oder Spanisch. Wir legen Wert auf eine verständliche Sprache, damit Sie wirklich wissen, woran Sie sind.

Geht es zum Beispiel ums Sorgerecht, um Trennung oder Scheidung, um Unterhaltsbeiträge? Oder um die rechtliche Gestaltung Ihrer Ehe oder Ihrer eingetragenen Partnerschaft? Haben Sie Ärger mit Ihrem Nachbarn oder der Vermieterin, Fragen zu Ihrem Miet­vertrag oder dem steigenden Mietzins? Sind Sie von einer Diskriminierung betroffen oder Opfer von Gewalt? Stehen Sie in Auseinander­setzung mit einer Versicherung wegen Krankheit oder Unfall? Haben Sie arbeitsrechtliche Fragen? Brauchen Sie Unterstützung im Verkehr mit den Migrationsbehörden, haben Sie Fragen zum Familiennachzug oder betreffend Ihre Aufenthalts- oder Niederlassungsbewilligung?

Längst nicht jede Rechtsfrage muss zu einem „Fall“ werden, längst nicht jeder Rechtsstreit wird vor Gericht ausgetragen. Manchmal reicht ein Gespräch, um Ihre Situation zu klären und aufzuzeigen, welche Wege Sie ein­schla­gen können. Wenn es aber um die Durch­setzung von berechtig­ten Rechtsan­sprüchen geht, vertreten wir Sie konsequent gegenüber den anderen Parteien. Damit Sie nicht nur recht haben, sondern auch zu Ihrem Recht kommen. Dies gilt für Sie als Frau, als Mann, als Jugendliche, als Jugendlicher. Explizit gilt dies auch für Kin­der, die in Trennungs- und Scheidungs­verfahren oder als Opfer von Gewaltdelikten eine eigenständige Vertretung brauchen.
