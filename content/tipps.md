---
title: "Tipps"
title-meta: "Tipps"
description: ""
header: head16.jpg
---

## Unterhaltsbeiträge

Unterhaltsbeiträge sind jeweils auf den 1. jeden Monats im Voraus zahlbar.

Werden Unterhaltsbeiträge nicht oder nicht regelmässig bezahlt kann beim Gericht eine Anweisung an den Arbeitgeber oder die Arbeitgeberin der unterhaltsverpflichteten Person oder an die Sozialversicherung beantragt werden.

Werden Unterhaltsbeiträge nicht oder nicht regelmässig bezahlt, können Sie sich für das Inkasso und die Bevorschussung der Alimente auch an die zuständige kantonale Stelle wenden:  
Amt für Sozialbeiträge, Alimentenhilfe, Grenzacherstrasse 62, Postfach, 4005 Basel, Tel. 061 267 56 60  
Kantonales Sozialamt Liestal, Tel. 061 552 56 45

Für eine Indexierung der Unterhaltsbeiträge wählen Sie:  
<a href="http://www.asb.bs.ch/familien/alimentenhilfe.html" target="_blank">http://www.asb.bs.ch/familien/alimentenhilfe.html</a>

## Unentgeltliche Prozessführung/ Kostenerlasszeugnis

Bei Bedürftigkeit und insofern Ihr Begehren nichts aussichtslos ist, kann Ihnen die unentgeltliche Prozessführung bewilligt werden. Wohnen Sie im Kanton Basel-Stadt, müssen Sie beim Sicherheitsdepartement (Bevölkerung und Migration, Spiegelgasse 6, Basel) das Kostenerlasszeugnis bestellen und der Anwältin übergeben.

Bei Wohnsitz im Kanton Basellandschaft können Sie dieses herunterladen und ausfüllen unter folgender Adresse: <a href="https://www.baselland.ch/politik-und-behorden/gerichte/oft-besuchte-seiten/informationen-und-formulare/downloads/up-form.pdf" target="_blank">PDF</a>
