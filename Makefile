SITE = public
TOOLS = tools
PANDOC_VERSION = 2.11.2
SASS_VERSION = 1.24.0

pandoc = $(TOOLS)/pandoc-$(PANDOC_VERSION)/bin/pandoc
pandoc_cmd = $(pandoc) --from=markdown-auto_identifiers
sass = $(TOOLS)/dart-sass/sass

content = $(patsubst content/%.md,%.html,$(wildcard content/*.md))
$(info content: $(content))

images = $(patsubst images/%,%,$(wildcard images/*.jpg)) 80.png alphabeet.gif loading.gif names-head.svg
$(info images: $(images))

site-files = $(content) \
    $(addprefix css/,main.css) \
    $(addprefix js/,mootools-1.4.5.js  html5-3.js  mediabox.js) \
    $(addprefix img/,$(images)) favicon.ico icon.png \
    googleeb2488027923d76d.html

.DEFAULT_GOAL := site
.PHONY: site clean clean-all

site: $(addprefix $(SITE)/,$(site-files))

clean:
	rm -rf $(SITE)

clean-all: clean
	rm -rf $(TOOLS)

$(pandoc):
	@mkdir -p $(TOOLS)
	wget -O $(TOOLS)/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
	tar -xzf $(TOOLS)/pandoc.tar.gz -C $(TOOLS)

$(sass):
	@mkdir -p $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/$(SASS_VERSION)/dart-sass-$(SASS_VERSION)-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(SITE)/%.html: content/%.md templates/template.html | $(pandoc)
	@mkdir -p $(@D)
	$(pandoc_cmd) --template=$(word 2,$^) --variable=path:$* --variable=nav-$* --output=$@ $<

$(SITE)/css/main.css: $(wildcard styles/*.scss) | $(sass)
	@mkdir -p $(@D)
	$(sass) styles/main.scss:$@

$(SITE)/js/%.js: scripts/%.js
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/img/%: images/%
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/favicon.ico $(SITE)/icon.png: $(SITE)/%: images/%
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/googleeb2488027923d76d.html: googleeb2488027923d76d.html
	@mkdir -p $(@D)
	cp $< $@
